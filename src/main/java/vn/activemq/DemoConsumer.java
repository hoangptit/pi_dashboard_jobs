package vn.activemq;

import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;

/**
 * Created by hoangtd on 9/20/2017.
 */
public class DemoConsumer {
    public static void main(String[] args) {
        try {
            ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://localhost:61616");

            Connection connection = connectionFactory.createConnection();
            connection.start();

            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

            Destination destination = session.createQueue("DEMOCONSUMER.TESTMQ");

            MessageConsumer consumer = session.createConsumer(destination);

            Message message  = consumer.receive(1000);

            if(message instanceof TextMessage){
                TextMessage textMessage = (TextMessage)message;
                String text = textMessage.getText();
                System.out.println("Recived: " + text);
            }else{
                System.out.println("Recived: " + message);
            }

            consumer.close();
            session.close();
            connection.close();
        }catch (Exception e){

        }
    }
}
