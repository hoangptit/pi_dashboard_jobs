package vn.activemq;

import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;

/**
 * Created by hoangtd on 9/20/2017.
 */
public class DemoProducer {
    public static void main(String[] args) {
        try {
            ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://localhost:61616");

            Connection connection = connectionFactory.createConnection();

            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

            Destination destination = session.createQueue("DEMOPRODUCER.TESTMQ");

            MessageProducer producer = session.createProducer(destination);
            producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);

            String text = "Hello From " + Thread.currentThread().getName();
            TextMessage message = session.createTextMessage(text);

            System.out.println("message: " + message.hashCode() + " : " + Thread.currentThread().getName());
            producer.send(message);

            session.close();;
            connection.close();
        }catch (Exception e){

        }
    }
}
